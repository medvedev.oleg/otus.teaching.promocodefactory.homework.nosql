using System.Runtime.CompilerServices;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.Controllers
{
    public interface ITaskAwaitable<T>
    {
        TaskAwaiter<T> GetAwaiter();
    }
}
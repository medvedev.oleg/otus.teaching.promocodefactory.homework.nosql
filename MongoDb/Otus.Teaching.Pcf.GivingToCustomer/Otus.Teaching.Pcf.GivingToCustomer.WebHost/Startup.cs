﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Castle.Core.Configuration;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Configuration;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Gateways;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.GivingToCustomer.DataAccess;
using Otus.Teaching.Pcf.GivingToCustomer.DataAccess.Data;
using Otus.Teaching.Pcf.GivingToCustomer.DataAccess.Repositories;
using Otus.Teaching.Pcf.GivingToCustomer.Integration;
using IConfiguration = Microsoft.Extensions.Configuration.IConfiguration;
using MongoDB.Driver;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using System.Threading.Channels;
using System.Threading;
using Castle.Core.Resource;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost
{
    public class Startup
    {
        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers().AddMvcOptions(x =>
                x.SuppressAsyncSuffixInActionNames = false);
            services.AddScoped(typeof(IRepository<>), typeof(EfRepository<>));
            services.AddScoped<INotificationGateway, NotificationGateway>();
            services.AddScoped<IDbInitializer, EfDbInitializer>();
            services.AddDbContext<DataContext>(x =>
            {
                //x.UseSqlite("Filename=PromocodeFactoryGivingToCustomerDb.sqlite");
                x.UseNpgsql(Configuration.GetConnectionString("PromocodeFactoryGivingToCustomerDb"));
                x.UseSnakeCaseNamingConvention();
                x.UseLazyLoadingProxies();
            });


            services
             .AddSingleton<IMongoClient>(_ => new MongoClient(Configuration.GetConnectionString("MongoDb:PromocodeFactoryGivingToCustomerDb")))
             .AddSingleton(serviceProvider => serviceProvider.GetRequiredService<IMongoClient>().GetDatabase(Configuration.GetConnectionString("MongoDb:DatabaseName")))
             .AddSingleton(serviceProvider => serviceProvider.GetRequiredService<IMongoDatabase>().GetCollection<Preference>("preferences"))
             .AddSingleton(serviceProvider => serviceProvider.GetRequiredService<IMongoDatabase>().GetCollection<Customer>("customers"))
             .AddSingleton(serviceProvider => serviceProvider.GetRequiredService<IMongoDatabase>().GetCollection<CustomerPreference>("customer_preference"))
             .AddSingleton(serviceProvider => serviceProvider.GetRequiredService<IMongoDatabase>().GetCollection<PromoCodeCustomer>("promo_code_customer"))
             .AddSingleton(serviceProvider => serviceProvider.GetRequiredService<IMongoDatabase>().GetCollection<PromoCode>("promo_codes"))
              .AddScoped(serviceProvider => serviceProvider.GetRequiredService<IMongoClient>().StartSession());

            services.AddTransient<ICustomerMongoDbRepository, CustomerMongoDbRepository>();
            services.AddTransient<IPreferenceMongoDbRepository, PreferenceMongoDbRepository>();
            services.AddTransient<IPromoCodeMongoDbRepository, PromoCodeMongoDbRepository>();


            services.AddOpenApiDocument(options =>
            {
                options.Title = "PromoCode Factory Giving To Customer API Doc";
                options.Version = "1.0";
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IDbInitializer dbInitializer)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseOpenApi();
            app.UseSwaggerUi3(x =>
            {
                x.DocExpansion = "list";
            });

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            // dbInitializer.InitializeDb();

            //Заполняем Монго БД начальными данными
            var mongoClient = new MongoClient(Configuration.GetConnectionString("MongoDb:PromocodeFactoryGivingToCustomerDb"));
            var mongoDb = mongoClient.GetDatabase(Configuration.GetConnectionString("MongoDb:DatabaseName"));

            //preference
            mongoDb.DropCollection("preferences");
            mongoDb.CreateCollection("preferences");
            var preferenceCollection = mongoDb.GetCollection<Preference>("preferences");
            preferenceCollection.InsertManyAsync(FakeDataFactory.Preferences);


            //customers
            mongoDb.DropCollection("customers");
            mongoDb.CreateCollection("customers");
            var customersCollection = mongoDb.GetCollection<Customer>("customers");
            customersCollection.InsertManyAsync(FakeDataFactory.Customers);

            //customer_prefence
            mongoDb.DropCollection("customer_preference");
            mongoDb.CreateCollection("customer_preference");
            var customerPreferenceCollection = mongoDb.GetCollection<CustomerPreference>("customer_preference");
            var fakeCustomer = FakeDataFactory.Customers.FirstOrDefault();
            //var fakePreferences = FakeDataFactory.Preferences;

            //IEnumerable<CustomerPreference> customerPreferences = fakeCustomer.Preferences.Select(p => new CustomerPreference()
            //{
            //    CustomerId = p.CustomerId,
            //    // Customer = fakeCustomer,
            //    PreferenceId = p.PreferenceId,
            //    // Preference = fakePreferences.Find(fp => fp.Id == p.PreferenceId)
            //});
            customerPreferenceCollection.InsertManyAsync(FakeDataFactory.CustomersPreferences(fakeCustomer));

            //customer_prefence
            mongoDb.DropCollection("promo_code_customer");
            mongoDb.CreateCollection("promo_code_customer");
            var promoCodeCustomerCollection = mongoDb.GetCollection<PromoCodeCustomer>("promo_code_customer");
            //promoCodeCustomerCollection.InsertManyAsync(FakeDataFactory.Customers.FirstOrDefault().Preferences);

            //customer_prefence
            mongoDb.DropCollection("promo_codes");
            mongoDb.CreateCollection("promo_codes");
            var promoCodesCollection = mongoDb.GetCollection<PromoCode>("promo_codes");
            //promoCodesCollection.InsertManyAsync(FakeDataFactory.Customers.FirstOrDefault().Preferences);

        }
    }
}
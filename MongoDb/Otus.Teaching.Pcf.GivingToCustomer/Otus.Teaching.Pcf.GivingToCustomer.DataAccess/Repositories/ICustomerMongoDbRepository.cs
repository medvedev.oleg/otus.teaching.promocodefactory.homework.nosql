using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.GivingToCustomer.DataAccess.Repositories
{
    public interface ICustomerMongoDbRepository
    {
        Task<List<Customer>> GetAllAsync();
 
        Task<Customer> GetByIdAsync(Guid id);

        Task<List<Customer>> GetRangeByIdsAsync(List<Guid> ids);

        Task<Customer> GetFirstWhere(Expression<Func<Customer, bool>> predicate);

        Task<List<Customer>> GetWhere(Expression<Func<Customer, bool>> predicate);

        Task AddAsync(Customer entity);

        Task UpdateAsync(Customer entity);

        Task DeleteAsync(Customer entity);
    }
}
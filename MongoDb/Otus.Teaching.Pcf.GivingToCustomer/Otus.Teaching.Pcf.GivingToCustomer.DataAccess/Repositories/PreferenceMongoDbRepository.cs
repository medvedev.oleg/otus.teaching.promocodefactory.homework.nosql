﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Driver;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;

namespace Otus.Teaching.Pcf.GivingToCustomer.DataAccess.Repositories
{
    public class PreferenceMongoDbRepository : IPreferenceMongoDbRepository
    {
        private readonly IMongoCollection<Preference> _preferenceCollection;

        public PreferenceMongoDbRepository(IMongoCollection<Preference> preferenceCollection)
        {
            _preferenceCollection = preferenceCollection;
        }

        /// <summary>
        /// </summary>
        /// <returns></returns>
        public async Task<List<Preference>> GetAllAsync()
        {
            var entity = await _preferenceCollection.Find(new BsonDocument()).ToListAsync();
            return entity;
        }
         
        public async Task<List<Preference>> GetRangeByIdsAsync(List<Guid> ids)
        {
            var entities = await _preferenceCollection.Find(x => ids.Contains(x.Id)).ToListAsync();
            return entities;
        }

        public async Task<Preference> GetByIdAsync(Guid id)
        {
            var entity = await _preferenceCollection.FindAsync(x => x.Id == id);
            return (Preference)entity;
        }

    }
}
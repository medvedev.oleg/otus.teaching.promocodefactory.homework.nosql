using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.GivingToCustomer.DataAccess.Repositories
{
    public interface IPromoCodeMongoDbRepository
    {
        Task<List<PromoCode>> GetAllAsync();
 
        Task<PromoCode> GetByIdAsync(Guid id);

        Task<List<PromoCode>> GetRangeByIdsAsync(List<Guid> ids);

        Task<PromoCode> GetFirstWhere(Expression<Func<PromoCode, bool>> predicate);

        Task<List<PromoCode>> GetWhere(Expression<Func<PromoCode, bool>> predicate);

        Task AddAsync(PromoCode entity);
         
        Task DeleteAsync(PromoCode entity);
    }
}
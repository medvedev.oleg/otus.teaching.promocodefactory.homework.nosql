using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.GivingToCustomer.DataAccess.Repositories
{
    public interface IPreferenceMongoDbRepository
    {
        Task<List<Preference>> GetAllAsync();

        Task<List<Preference>> GetRangeByIdsAsync(List<Guid> ids);

        Task<Preference> GetByIdAsync(Guid id);

    }
}
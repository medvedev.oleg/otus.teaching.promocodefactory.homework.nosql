﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Driver;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using static Microsoft.EntityFrameworkCore.DbLoggerCategory;

namespace Otus.Teaching.Pcf.GivingToCustomer.DataAccess.Repositories
{
    public class PromoCodeMongoDbRepository : IPromoCodeMongoDbRepository
    {
        private readonly IMongoCollection<PromoCode> _promoCodeCollection;

        public PromoCodeMongoDbRepository(IMongoCollection<PromoCode> promoCodeCollection)
        {
            _promoCodeCollection = promoCodeCollection;
        }

        /// <summary>
        /// </summary>
        /// <returns></returns>
        public async Task<List<PromoCode>> GetAllAsync()
        {
            var entity = await _promoCodeCollection.Find(new BsonDocument()).ToListAsync();
            return entity;
        }

        public async Task<PromoCode> GetByIdAsync(Guid id)
        {
            var entity = await _promoCodeCollection.FindAsync(x => x.Id == id);
            return (PromoCode)entity;
        }

        public async Task<PromoCode> GetFirstWhere(Expression<Func<PromoCode, bool>> predicate)
        {
            return await _promoCodeCollection.Find(predicate).FirstAsync();
        }

        public async Task<List<PromoCode>> GetRangeByIdsAsync(List<Guid> ids)
        {
            var entities = await _promoCodeCollection.Find(x => ids.Contains(x.Id)).ToListAsync();
            return entities;
        }

        public async Task<List<PromoCode>> GetWhere(Expression<Func<PromoCode, bool>> predicate)
        {
            return await _promoCodeCollection.Find(predicate).ToListAsync();
        }

        public async Task AddAsync(PromoCode entity)
        {
            await _promoCodeCollection.InsertOneAsync(entity);
        }
         

        public async Task DeleteAsync(PromoCode entity)
        {
            var filter = Builders<PromoCode>.Filter.Eq("Id", entity.Id);
            await _promoCodeCollection.DeleteOneAsync(filter);

        }

    }
}
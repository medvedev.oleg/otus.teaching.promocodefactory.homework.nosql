﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Driver;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using static Microsoft.EntityFrameworkCore.DbLoggerCategory;

namespace Otus.Teaching.Pcf.GivingToCustomer.DataAccess.Repositories
{
    public class CustomerMongoDbRepository : ICustomerMongoDbRepository
    {
        private readonly IMongoCollection<Customer> _customerCollection;

        public CustomerMongoDbRepository(IMongoCollection<Customer> customerCollection)
        {
            _customerCollection = customerCollection;
        }

        /// <summary>
        /// </summary>
        /// <returns></returns>
        public async Task<List<Customer>> GetAllAsync()
        {
            var entity = await _customerCollection.Find(new BsonDocument()).ToListAsync();
            return entity;
        }

        public async Task<Customer> GetByIdAsync(Guid id)
        {
            var entity = await _customerCollection.Find(x => x.Id == id).ToListAsync();
            return entity.FirstOrDefault();
        }

        public async Task<Customer> GetFirstWhere(Expression<Func<Customer, bool>> predicate)
        {
            return await _customerCollection.Find(predicate).FirstAsync();
        }

        public async Task<List<Customer>> GetRangeByIdsAsync(List<Guid> ids)
        {
            var entities = await _customerCollection.Find(x => ids.Contains(x.Id)).ToListAsync();
            return entities;
        }

        public async Task<List<Customer>> GetWhere(Expression<Func<Customer, bool>> predicate)
        {
            return await _customerCollection.Find(predicate).ToListAsync();
        }

        public async Task AddAsync(Customer entity)
        {
            await _customerCollection.InsertOneAsync(entity);
        }


        public async Task UpdateAsync(Customer entity)
        {
            var filter = Builders<Customer>.Filter.Eq("Id", entity.Id);
            var update = Builders<Customer>.Update
                .Set(c => c.FirstName, entity.FirstName)
                .Set(c => c.LastName, entity.LastName)
                .Set(c => c.Email, entity.Email)
                .Set(c => c.Preferences, entity.Preferences)
                .Set(c => c.PromoCodes, entity.PromoCodes);

            await _customerCollection.UpdateOneAsync(filter, update, new UpdateOptions { IsUpsert = true });
        }

        public async Task DeleteAsync(Customer entity)
        {
            var filter = Builders<Customer>.Filter.Eq("Id", entity.Id);
            await _customerCollection.DeleteOneAsync(filter);

        }

    }
}